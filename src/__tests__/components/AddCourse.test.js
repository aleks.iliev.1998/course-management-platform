import { render, screen } from '@testing-library/react';
import AddCourse from '../../components/course/AddCourse';

test('Renders Successfully', () => {
    render(<AddCourse location='' />);
    const titleInput = screen.getByTestId('title');
    const descriptionInput = screen.getByTestId('description')
    const submitBtn = screen.getByTestId('submitBtn');
    expect(titleInput).toBeInTheDocument();
    expect(descriptionInput).toBeInTheDocument();
    expect(submitBtn).toBeInTheDocument();
})