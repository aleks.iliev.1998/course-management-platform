import axios from 'axios';
import uuid from 'react-uuid';
import API_URL from '../../constants/course/API_URL';
import courseService from '../../service/CourseService'

var course;

const getCourse = () => {
    return {
        id: undefined,
        title: 'New Course',
        description: 'dfsgfdsgdg',
        testId: uuid()
    }
}

beforeEach(() => {
    return axios.get(API_URL)
        .then((resp) => expect(resp.data).not.toContain(getCourse()))
        .catch((err) => console.log(err));
})

afterEach(() => {
    return axios.delete(API_URL + `/${course.id}`).catch((err) => console.log(err));
})

test('Adds New Course', () => {

    let matchFound = false;
    course = getCourse();

    courseService.addCourse(course).then(() => {
        return axios.get(API_URL).then((resp) => {
            resp.data.forEach(c => {
                if (c.id === course.id) {
                    matchFound = true;
                }
            });
        }).then(() => expect(matchFound).toBeTruthy())
    })

})

test('Deletes Existing Course', () => {
    course = getCourse();
    course.id = uuid();
    return axios.post(API_URL, course).then(() => {
        axios.get(API_URL).then((resp) =>
            expect(resp.data.filter(c => c.id === course.id)).toHaveLength(1))
            .then(() => {
                courseService.deleteCourse(course).then(() => {
                    axios.get(API_URL).then((resp) => {
                        let result = resp.data.filter(c => c.id === course.id);
                        expect(result.length).toBe(0);
                    })
                });
            });

    });
})