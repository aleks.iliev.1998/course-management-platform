import ERROR_MESSAGE from '../../constants/course/REQUIRED_ERROR_MESSAGE';
import InputValidator from '../../validation/InputValidator'

test('Returns error message with null input', () => {
    expect(InputValidator.isNullorEmpty(null)).toBe(true);
})

test('Returns error message with undefined input', () => {
    expect(InputValidator.isNullorEmpty(undefined)).toBe(true);
})

test('Returns error message with empty input', () => {
    expect(InputValidator.isNullorEmpty('')).toBe(true);
})

test('Returns error message with space input', () => {
    expect(InputValidator.isNullorEmpty(' ')).toBe(true);
})

test('Returns null with valid input', () => {
    expect(InputValidator.isNullorEmpty('a')).toBe(false);
    expect(InputValidator.isNullorEmpty('aa')).toBe(false);
    expect(InputValidator.isNullorEmpty('a a')).toBe(false);
}) 