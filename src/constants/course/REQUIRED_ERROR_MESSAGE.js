const REQUIRED_ERROR_MESSAGE = "This field is required!"

export default REQUIRED_ERROR_MESSAGE;