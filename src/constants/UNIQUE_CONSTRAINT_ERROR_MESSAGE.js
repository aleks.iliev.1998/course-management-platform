const UNIQUE_CONSTRAINT_ERROR_MESSAGE = 'A course with this title already exists!'

export default UNIQUE_CONSTRAINT_ERROR_MESSAGE 