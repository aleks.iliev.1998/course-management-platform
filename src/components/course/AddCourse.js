import React, { Component } from 'react'
import { Alert, Button, Card, Col, Form, Image, Row } from 'react-bootstrap'
import { Redirect } from 'react-router-dom';
import CourseService from '../../service/CourseService';
import InputValidator from '../../validation/InputValidator';
import ImageUploader from 'react-images-upload';
import REQUIRED_ERROR_MESSAGE from '../../constants/course/REQUIRED_ERROR_MESSAGE';
import UNIQUE_CONSTRAINT_ERROR_MESSAGE from '../../constants/UNIQUE_CONSTRAINT_ERROR_MESSAGE';

export default class AddCourse extends Component {

    constructor(props) {
        super(props);

        this.state = {
            redirect: false,
            id: undefined,
            title: '',
            description: '',
            errorMessage: '',
            imageURL: undefined,
            testId: undefined,
            existingCourses: []
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.onDrop = this.onDrop.bind(this);
        this.getCourses = this.getCourses.bind(this);
    }

    componentDidMount() {
        if (this.props.location.state) {
            let course = this.props.location.state.course;
            this.setState({
                id: course.id,
                title: course.title,
                description: course.description,
                imageURL: course.imageURL
            })
        }
        this.getCourses();

    }

    getCourses() {
        return CourseService.getCourses().then((resp) => {
            this.setState({
                existingCourses: resp.data
            });
        });
    }

    onDrop(event) {
        this.setState({
            imageURL: URL.createObjectURL(event[0])
        });
    }


    handleSubmit(e, testContext) {
        e.preventDefault();
        if (!InputValidator.isNullorEmpty(this.state.title)
            && InputValidator.isUnique(this.state.existingCourses, this.state.title, this.state.id)) {
            CourseService.addCourse({
                id: this.state.id,
                title: this.state.title,
                description: this.state.description,
                imageURL: this.state.imageURL,
                testId: this.state.testId
            }).then(() => {
                if (!testContext) {
                    this.setState({
                        redirect: true
                    })
                }

            });
        } else if (InputValidator.isNullorEmpty(this.state.title)) {
            this.setState({
                errorMessage: REQUIRED_ERROR_MESSAGE
            });
        } else if (!InputValidator.isUnique(this.state.existingCourses, this.state.title, this.state.id)) {
            this.setState({
                errorMessage: UNIQUE_CONSTRAINT_ERROR_MESSAGE
            })
        }
    }

    render() {

        if (this.state.redirect) {
            return <Redirect to='/' />
        }

        return (
            <Row>
                <Col>
                    <Card className='mt-3'>
                        <Card.Title className='m-4'>
                            Add Course
                        </Card.Title>
                        <Card.Body>
                            <Form onSubmit={this.handleSubmit}>
                                <Form.Group className="mb-3" controlId="title">
                                    <Form.Label>Title*</Form.Label>
                                    <Form.Control className='title'
                                        data-testid='title'
                                        type="text" placeholder="Enter title"
                                        value={this.state.title}
                                        onChange={(e) => this.setState({
                                            title: e.target.value
                                        }, () => {
                                            console.log(this.state.courseTitles);
                                            if (InputValidator.isNullorEmpty(this.state.title)) {
                                                this.setState({
                                                    errorMessage: REQUIRED_ERROR_MESSAGE
                                                });
                                            } else if (!InputValidator.isUnique(this.state.existingCourses, this.state.title, this.state.id)) {
                                                this.setState({
                                                    errorMessage: UNIQUE_CONSTRAINT_ERROR_MESSAGE
                                                });
                                            } else {
                                                this.setState({
                                                    errorMessage: ''
                                                })
                                            }

                                        })} />
                                    {this.state.errorMessage &&
                                        <Alert data-test-id='alert' className='mt-2 fw-bold' variant='danger'>{this.state.errorMessage}</Alert>}
                                </Form.Group>

                                <Form.Group className="mb-3" controlId="description">
                                    <Form.Label>Description</Form.Label>
                                    <Form.Control as='textarea' type="text" data-testid="description"
                                        className='description'
                                        value={this.state.description}
                                        onChange={(e) => this.setState({
                                            description: e.target.value
                                        })} />
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="image">
                                    <Image src={this.state.imageURL} width='100%' height='auto' />
                                    <ImageUploader
                                        withIcon={true}
                                        withLabel={false}
                                        buttonText='Choose Image'
                                        onChange={this.onDrop}
                                        imgExtension={['.jpg', '.gif', '.png', '.gif']}
                                        maxFileSize={5242880}
                                    />

                                </Form.Group>
                                <Button data-testid="submitBtn"
                                    className='submitBtn'
                                    variant="primary" type="submit">
                                    Submit
                                </Button>
                                <Button variant='link' type='button' onClick={() => this.setState({
                                    redirect: true
                                })}>Cancel</Button>
                            </Form>
                        </Card.Body>
                    </Card>
                </Col>
                <Col></Col>
            </Row>
        )
    }
}
