import React, { Component } from 'react'
import { Alert, Button, Image } from 'react-bootstrap';
import ADD_COURSE_PATH from '../../constants/course/ADD_COURSE_PATH';
import { Redirect } from "react-router-dom";
import CourseService from '../../service/CourseService';
import Modal from 'react-modal';

export default class Course extends Component {

    constructor(props) {
        super(props);
        this.state = {
            course: this.props.course,
            deleteModalOpen: false,
            redirectToEdit: false
        }
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleDelete(id) {
        CourseService.deleteCourse(id).then(() => this.setState({
            deleteModalOpen: false
        })).then(() => this.props.updateCourses());
    }

    render() {
        if (this.state.redirectToEdit) {
            return <Redirect to={{
                pathname: ADD_COURSE_PATH,
                state: {
                    course: this.state.course
                }
            }} />
        }

        return (
            <tr data-testid='course'>
                {this.state.deleteModalOpen &&
                    <Modal isOpen={this.state.deleteModalOpen} onRequestClose={() => this.setState({
                        deleteModalOpen: false
                    })} style={
                        {
                            content: {
                                top: '50%',
                                left: '50%',
                                right: 'auto',
                                bottom: 'auto',
                                marginRight: '-50%',
                                transform: 'translate(-50%, -50%)',
                            }
                        }
                    }>
                        <Alert variant='danger'>Are you sure you want to delete the course
                            <b> {this.props.title}</b>? </Alert>

                        <Button variant='danger' onClick={() => this.handleDelete(this.state.course.id)}>Yes, Delete</Button>
                        <Button variant='link' onClick={() => this.setState({
                            deleteModalOpen: false
                        })}>Cancel</Button>
                    </Modal>}
                <td data-test-id={this.state.course.title} className='fw-bold text-center'>{this.state.course.title}</td>
                <td data-test-id={this.state.course.description} className='text-center'>{this.state.course.description}</td>
                <td>{this.state.course.imageURL &&
                    <Image width='30%' src={this.state.course.imageURL} />}</td>
                <td><Button data-test-id={'delete-' + this.state.course.title} variant='danger' onClick={() => this.setState({
                    deleteModalOpen: true
                })}>Delete</Button></td>
                <td><Button data-test-id={'edit-' + this.state.course.title} variant='primary' onClick={() => this.setState({
                    redirectToEdit: true
                })}>Edit</Button></td>
            </tr>
        )
    }
}
