import React, { Component } from 'react'
import { Alert, Table } from 'react-bootstrap';
import CourseService from '../../service/CourseService';

import Course from './Course';

export default class Courses extends Component {

    constructor(props) {
        super(props);
        this.state = {
            courses: []
        }
        this.updateCourses = this.updateCourses.bind(this);
    }

    updateCourses() {
        CourseService.getCourses().then((resp) => this.setState({
            courses: resp.data
        }));
    }

    componentDidMount() {
        this.updateCourses();

    }

    render() {

        return (
            <div>
                {this.state.courses.length > 0 ? (
                    <Table responsive>
                        <tbody>
                            {
                                this.state.courses.map((course) =>
                                    <Course key={course.id} updateCourses={this.updateCourses}
                                        course={course} />
                                )
                            }
                        </tbody>
                    </Table>
                ) : (
                    <Alert className='fw-bold' variant='info'>There are currently no courses.</Alert>
                )}

            </div>
        )
    }
}
