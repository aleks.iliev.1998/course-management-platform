
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';
import AddCourse from './components/course/AddCourse';
import Courses from './components/course/Courses';

import Modal from 'react-modal';
import React, { Component } from 'react'
import ADD_COURSE_PATH from './constants/course/ADD_COURSE_PATH';
import { Container, Nav, Navbar } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default class App extends Component {

  constructor(props) {
    super(props);
    Modal.setAppElement('#root');
  }

  render() {
    return (
      <Router>
        <Container>
          <Navbar bg="light" expand="lg">
            <Container>
              <Navbar.Brand>Course Manager</Navbar.Brand>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                  <Link className='text-decoration-none text-muted me-2' to='/' >View Courses</Link>
                  <Link className='text-decoration-none text-muted' to={ADD_COURSE_PATH} >Add Course</Link>
                </Nav>
              </Navbar.Collapse>
            </Container>
          </Navbar>
          <Route exact path='/' component={Courses} />
          <Route exact path={ADD_COURSE_PATH} component={AddCourse} />
        </Container>
      </Router>
    );
  }

}

