
class InputValidator {

    isNullorEmpty(value) {
        if (!value) {
            return true;
        }

        if (!value.toString().trim().length) {
            return true;
        }
        return false;
    }

    isUnique(existingCourses, title, courseId) {
        let matches = existingCourses.filter(c => c.title === title && c.id !== courseId);
        if (matches.length > 0) {
            return false;
        }
        return true;
    }

}

export default new InputValidator()