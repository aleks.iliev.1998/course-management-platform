import axios from "axios";
import uuid from "react-uuid";
import API_URL from "../constants/course/API_URL";

class CourseService {

    addCourse(course) {
        if (!course.id) {
            course.id = uuid();
            return axios.post(API_URL, course);
        } else {
            return axios.put(API_URL + `/${course.id}`, course);
        }
    }

    deleteCourse(id) {
        return axios.delete(API_URL + `/${id}`);
    }

    getCourses = () => {
        return axios.get(API_URL);
    }

}

export default new CourseService()