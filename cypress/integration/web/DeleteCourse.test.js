
import BASE_PATH from "../../../src/constants/course/BASE_PATH";
import CourseService from "../../../src/service/CourseService";

var course = {
    title: 'New Course',
    description: 'dfhfjfgjgfjsgdsfd'
};

beforeEach(() => {
    course = {
        title: 'New Course',
        description: 'dfhfjfgjgfjsgdsfd'
    }
    CourseService.getCourses().then((resp) => {
        let result = resp.data.filter(c => c.title === course.title)
        if (result.length === 0) {
            CourseService.addCourse(course);
        }
    });
})

const testId = `[data-test-id="delete-${course.title}"]`;

afterEach(() => {
    CourseService.deleteCourse(course.id).catch((err) => console.log(err));
})

it('Should Delete Course', () => {
    cy.visit(BASE_PATH);
    cy.get(testId).click();
    cy.contains('Yes, Delete').click();
    cy.wait(100);
    cy.get(testId).should('not.exist');
});

it('Should Not Delete Course', () => {
    cy.visit(BASE_PATH);
    cy.get(testId).click();
    cy.contains('Cancel').click();
    cy.wait(100);
    cy.get(testId).should('exist');
})
