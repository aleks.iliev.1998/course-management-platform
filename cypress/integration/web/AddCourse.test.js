
import axios from "axios";
import API_URL from "../../../src/constants/course/API_URL";
import BASE_PATH from "../../../src/constants/course/BASE_PATH";
import REQUIRED_ERROR_MESSAGE from "../../../src/constants/course/REQUIRED_ERROR_MESSAGE";
import UNIQUE_CONSTRAINT_ERROR_MESSAGE from "../../../src/constants/UNIQUE_CONSTRAINT_ERROR_MESSAGE";

const course = {
    title: 'New Course',
    description: 'dfhfjfgjgfjsgdsfd'
}

var courses;

const clearCourse = () => {
    return axios.get(API_URL).then((resp) => {
        let result = resp.data.filter(c => c.title === course.title);
        if (result.length > 0) {
            return axios.delete(API_URL + `/${result[0].id}`).catch((err) => console.log(err));
        }
    });
}

const getExistingCourse = () => {
    return courses[0];
}

const attemptAddCourse = (shouldClearInput, shouldTypeExistingTitle) => {
    cy.visit(BASE_PATH);
    cy.contains('Add Course').click();
    let titleInput = cy.get('.title').type(course.title);
    if (shouldClearInput || shouldTypeExistingTitle) {
        titleInput.clear();
    }
    if (shouldTypeExistingTitle) {

        cy.get('.title').type(getExistingCourse().title);
    }
    cy.get('.description').type(course.description);
    cy.get('.submitBtn').click();
}

const attemptEditCourse = (attrName) => {
    let existingCourse = getExistingCourse();
    console.log(existingCourse);
    cy.visit(BASE_PATH);
    cy.get(`[data-test-id="edit-${existingCourse.title}"]`).click();
    let strToConcat = 'abc';
    let newAttr = existingCourse[attrName] + strToConcat;
    cy.get(`.${attrName}`).type(strToConcat);
    cy.get('.submitBtn').click();
    cy.get(`[data-test-id="${newAttr}"]`).should('exist');
}

beforeEach(() => {
    return axios.get(API_URL).then((resp) => courses = resp.data);
})

afterEach(() => {
    clearCourse();
})

it('Should Add Course Successfully', () => {
    attemptAddCourse(false, false);
    cy.get(`[data-test-id="${course.title}"]`).should('have.text', course.title);
})

it('Should Not Add Course & Should Display Empty Error Message', () => {
    attemptAddCourse(true, false);
    cy.get('[data-test-id="alert"]').should('have.text', REQUIRED_ERROR_MESSAGE);
})

it('Should Not Add Course & Should Display Unique Constraint Error Message', () => {
    attemptAddCourse(false, true);
    cy.get('[data-test-id="alert"]').should('have.text', UNIQUE_CONSTRAINT_ERROR_MESSAGE);
})

it('Should Update Course Title', () => {
    attemptEditCourse('title');
})

it('Should Update Course Description', () => {
    attemptEditCourse('description');
})